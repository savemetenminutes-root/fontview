<?php

declare(strict_types=1);

use Smtm\Fontview\Fontview;

chdir(__DIR__);
if(is_file('vendor/autoload.php')) {
    require 'vendor/autoload.php';
}

// Application start
$input = $_GET;

$stylesheetRemotePath = '';
$stylesheetLocalPath = null;
if (isset($input['stylesheet'])) {
    $stylesheetRemotePath = $input['stylesheet'];
    $stylesheet = file_get_contents($stylesheetRemotePath);
    $stylesheetLocalPath = __DIR__ . '/_tempstylesheet';
    file_put_contents($stylesheetLocalPath, $stylesheet);
}

$fullClassNames = null;
$fullClassNamesChecked = '';
if (isset($input['fullClassNames'])) {
    $fullClassNames = (bool) $input['fullClassNames'];
    $fullClassNamesChecked = ' checked="checked"';
}

$content = '';
$fontRemotePath = '';
if (isset($input['font'])) {
    $fontRemotePath = $input['font'];
    $font = file_get_contents($fontRemotePath);
    $fontLocalPath = __DIR__ . '/_tempfont';
    file_put_contents($fontLocalPath, $font);

    $fontview = new Fontview();
    $content = $fontview->renderGlyphsHtml($fontLocalPath, $stylesheetLocalPath, $fullClassNames);
}

echo <<< EOT
<!DOCTYPE html>
<head>
    <title>SaveMeTenMinutes - Fontview</title>
</head>
<body>
    <div class="content">
        <form action="" method="get">
            <a href="javascript:void(0);" onclick="document.getElementById('font').value = this.innerHTML;">http://msrc-nginx:81/fonts/fontawesome-webfont.ttf</a>
            <div>Font Path/URL:&nbsp;<input id="font" type="text" name="font" value="{$fontRemotePath}" style="width: 80%; box-sizing: border-box;"/></div>
            <div>Stylesheet Path/URL:&nbsp;<input id="stylesheet" type="text" name="stylesheet" value="{$stylesheetRemotePath}" style="width: 80%; box-sizing: border-box;"/></div>
            <div>Full Stylesheet Class Names:&nbsp;<input id="fullClassNames" type="checkbox" name="fullClassNames" value="1"$fullClassNamesChecked/></div>
            <input type="submit"/>
        </form>
        <div id="preview" class="preview">{$content}</div>
    </div>
</body>
EOT;



